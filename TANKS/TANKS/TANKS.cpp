

#include "stdafx.h"
#include <iostream>
#include <string>
#include <conio.h>
#include <chrono>
#include <thread>
#include <Windows.h>
#include <time.h>
#include <stdlib.h>
#include <ctime>
#include <fstream>
#include <sstream>
#include <math.h>

using namespace std;

const int hight=40;
const int width=120;
const int TANKSPEED=100;
const int BULLETSPEED=TANKSPEED/4;
const char b=(char)254;// bullet sign
const char ajor=(char)176;//divar ajor
const char beton=(char)178;// divar beton
const char SLIDER='$';//inorder to seprate detail in saved file
int N=0;//for internal purpose
int gamelvl=1;
bool endgame=false;
HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);
inline void gotoXY(int x, int y) {

	//Initialize the coordinates

	COORD coord = {x, y};

	//Set the position

	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);

	return;

}
inline void getCursorXY(int &x, int &y) {

	CONSOLE_SCREEN_BUFFER_INFO csbi;

	if(GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi)) {

		x = csbi.dwCursorPosition.X;

		y = csbi.dwCursorPosition.Y;
	}

}

enum direction
{
	up=72,
	down=80,
	_left=75,
	_right=77,
	shoot=32,
	pause='p',
	menu='q',
	save='s',
	NULL_dir=0,
};
//obvios
enum ownership
{
	player=1,
	pc=2
};
//obvios
enum color
{
	blue=9,
	black=0,
	green=10,
	white=7,
	red=12,
	lightblue=11,
	yellow=14,
	gray=8,
	purple=13,
};
//obvios
enum SpecialA
{
	secondchance='S',
	life='L',
	DS='d',
	range='R',
	NULL_SA='0'
};
//obvios
inline void setcolor(color back,int text)
{
	SetConsoleTextAttribute(hstdout,  16 *back+ text);
}

class keys
{
public:
	char up;
	char down;
	char _left;
	char _right;
	char shoot;
	int tankcol;
	keys()
	{
		up=72;
		down=80;
		_left=75;
		_right=77;
		shoot=32;
		tankcol=green;
	}
};
//obvios
class tank
{
public:
	string ID;
	bool alive;
	int life;
	int secondchance;
	int DS;//distruction power
	int range;
	int rangeR;//remained range
	int x;
	int y;
	int point;
	color tankcol;
	direction DR;//shape direction 
	direction MDR;//move direction
	ownership ownership;
	tank* address; 
	char temp;

	direction bulletD;//bullet direction
	bool bullet;//has bullet or not
	int bx,by;//bullet x and bullet y

	tank()
	{
		secondchance=3;
		life=100;
		DS=40;
		range=20;
		rangeR=range;
		bullet=false;
		alive=true;
		temp=' ';
		point=0;
	}
	void setdefault()
	{
		alive=true;
		secondchance=3;
		life=100;
		DS=40;
		range=20;
		bullet=false;
		temp=' ';
		point=0;

	}
	bool beontank(int xo,int yo);
	void cleartank();
	void printtank();
	void clearB(char arr [hight][width]);

	bool canrotate(direction D,tank* yourtank,tank* pc1,tank* pc2,tank* pc3,char arr [hight][width]);
	direction getrandomD(tank* yourtank,tank* pc1,tank* pc2,tank* pc3,char arr [hight][width]);
	//void go(char arr [hight][width],feild f1,direction D,tank* yourtank,tank* pc1,tank* pc2,tank* pc3);
	void clearwall(int WX,int WY,char arr [hight][width]);
	inline void setposition(int inpx,int inpy);
	void printB(char arr [hight][width]);
	void setB(char arr [hight][width],tank* yourtank,tank* pc1,tank* pc2,tank* pc3);
	void shoot(char arr [hight][width],tank* yourtank,tank* pc1,tank* pc2,tank* pc3);
	bool randomshoot(char arr [hight][width],tank* yourtank,tank* pc1,tank* pc2,tank* pc3);
	void kill(char arr [hight][width]);

};
//obvios
class feild
{
public:

	char arr [hight][width];
	//SA sa;
	int N_SC;//number of second chances
	int N_DS;// number of destrction power accelrattion abilities
	time_t T_DS; // time of puttting DS
	int available;
	int dsx;
	int dsy;
	feild()
	{

		available=0;
		N_SC=0;
		N_DS=0;
	}
	void print(tank* yourtank,tank* pc1 ,tank* pc2 ,tank* pc3 )
	{
		for(int i=0;i<hight;i++)
		{
			for(int j=0;j<width;j++)
			{
				if(yourtank->beontank(j,i))
				{
					setcolor(yourtank->tankcol,0);
				}
				if(pc1->beontank(j,i))
				{
					setcolor(pc1->tankcol,0);
				}
				if(pc2->beontank(j,i))
				{
					setcolor(pc2->tankcol,0);
				}
				if(pc3->beontank(j,i))
				{
					setcolor(pc3->tankcol,0);
				}
				cout<<arr[i][j];
				setcolor(black,white);
			}
			cout<<endl;
		}
	}


	void putSA(tank* yourtank,tank* pc1,tank* pc2,tank* pc3)
	{
		int x,y;
		do
		{
			x=rand()%(width-1)+1;
			y=rand()%(hight-1)+1;

		}
		while(arr[y][x]==ajor||arr[y][x]==beton || yourtank->beontank(x,y) || pc1->beontank(x,y)|| pc2->beontank(x,y)|| pc3->beontank(x,y));


		SpecialA SA=NULL_SA;
			int a=rand();
		do
		{

			
				int random=a++%10;
			if( random == 0 && N_SC < 1 )
			{
				SA=secondchance;
				N_SC++;
				available++;
			}
			else if ( random > 0 && random < 3 )
			{
				SA=life;
				available++;
			}
			else if (random  > 2 && random < 6 && N_DS < 1)
			{

				SA=DS;
				available++;
				N_DS++;
				T_DS=time(NULL);//put the cuurent time into T_DS			
				dsx=x;
				dsy=y;
			}
			else if (random>5 && random<10)
			{
				SA=range;
				available++;

			}
		}
		while(SA==NULL_SA);





		arr[y][x]=(char)SA;
		int CX,CY;
		getCursorXY(CX,CY);
		gotoXY(x,y);
		cout<<arr[y][x];
		gotoXY(CX,CY);





	}
	bool performA(tank* tank,SpecialA sa)//perform ability
	{
		if (sa == NULL_SA)
		{
			return false;
		}
		if (sa == secondchance && tank->ownership==player)
		{
			tank->secondchance+=1;
			N_SC--;
			available--;
			tank->point+=5;
			return true;
		}
		else if (sa == life)
		{
			tank->life+=50;
			available--;
			tank->point+=5;
			return true;
		}
		else if (sa == DS)
		{
			tank->DS*=2;
			N_DS--;
			available--;
			tank->point+=5;
			return true;
		}
		else if (sa==range)
		{
			tank->range+=5;
			available--;
			tank->point+=5;
			return true;
		}

	}
	SpecialA getSA(int x,int y)
	{
		if( (SpecialA) arr[y][x] == secondchance)
		{
			return secondchance;
		}
		else if( (SpecialA) arr[y][x] == DS)
		{
			return DS;
		}
		else if( (SpecialA) arr[y][x] == life)
		{
			return life;
		}
		else if( (SpecialA) arr[y][x] == range)
		{
			return range;
		}
		else 
		{
			return NULL_SA;
		}

	}
	void gotolvl2(tank* yourtank,tank* pc1 ,tank* pc2 ,tank* pc3)
	{
		N_SC=0;
		N_DS=0;
		available=0;
		ifstream maplvl2 ("maplvl2.txt");

		for(int j=0;!maplvl2.eof();j++)
		{
			maplvl2.get(arr[j/width][j%width]);//send the file ditails to array

		} 
		maplvl2.close();

		yourtank->setdefault();
		pc1->setdefault();
		pc2->setdefault();
		pc3->setdefault();
		/*for(int i=0;i<hight;i++)
		{
		for(int j=0;j<width;j++)
		{
		if(i==0 || j==0 ||  i==hight-1 ||  j==width-1)
		{
		arr[i][j]=beton;
		}
		else if((j==width/5 && i>0 && i<3*hight/4)||(j==4*width/5 && i>hight/4 && i<hight-1 )||
		(j==2*width/5 && i>hight/4 && i<hight-1 )||(j==3*width/5 && i>0 && i<3*hight/4))
		{
		arr[i][j]=ajor;

		}
		else
		{
		arr[i][j]=' ';
		}
		}
		}*/
		//	available=0;
		//N_SC=0;
		//N_DS=0;




	}
	void gotolvl3(tank* yourtank,tank* pc1 ,tank* pc2 ,tank* pc3)
	{
		N_SC=0;
		N_DS=0;
		available=0;

		ifstream maplvl3 ("maplvl3.txt");

		for(int j=0;!maplvl3.eof();j++)
		{
			maplvl3.get(arr[j/width][j%width]);//send the file ditails to array

		} 
		maplvl3.close();

		yourtank->setdefault();
		pc1->setdefault();
		pc2->setdefault();
		pc3->setdefault();
		available=0;
		N_SC=0;
		N_DS=0;




	}
};
//obvios
class load
{
public:
	int indx;
	int value;
	load()
	{
		indx=0;
		value=0;

	}


};
//class for handlig the loading
void gameframes(direction dir,feild* f1,tank* yourtank,tank* pc1,tank* pc2,tank* pc3,bool deafault=false);
//function which takes the moves of every tank and bullet
void puttanks(feild* f1,tank* yourtank,tank* pc1,tank* pc2,tank* pc3)
{
	int x,y;

	tank* tanks[4]={yourtank,pc1,pc2,pc3};

	for ( int i=0; i<4; i++)
	{
		bool flag;
		tank temp;
		do
		{
			flag=false;
			int a=rand()%4;
			switch (a)
			{
			case 0:
				temp.DR=up;
				break;	
			case 1:
				temp.DR=down;
				break;	
			case 2:
				temp.DR=_left;
				break;	
			case 3:
				temp.DR=_right;
				break;	
			default:
				break;
			}

			x=rand()%(width-3)+2;
			y=rand()%(hight-3)+2;

			temp.setposition(x,y);

			for(int m = x-2 ; m < x + 3 ; m++)
			{

				for(int j = y-2 ; j < y + 3 ; j++)
				{
					if( ( f1->arr[j][m]==ajor || f1->arr[j][m]==beton ) )//temp.beontank(x,y) &&
					{
						flag=true;


					}
					if(temp.beontank(m,j) &&  (yourtank->beontank(m,j)|| pc1->beontank(m,j) || pc2->beontank(m,j)|| pc3->beontank(m,j) ))
					{
						flag=true;


					}

				}

			}

		}
		while(flag || ( f1->arr[y][x]==ajor || f1->arr[y][x]==beton ||
			yourtank->beontank(x,y)|| pc1->beontank(x,y) || pc2->beontank(x,y)|| pc3->beontank(x,y) ));
		tanks[i]->setposition(x,y);
		tanks[i]->DR=temp.DR;

	}


}
//puts the yanks at the game start
void puttank(tank* ptank,feild* f1,tank* yourtank,tank* pc1,tank* pc2,tank* pc3)
{
	int x,y;

	bool flag;
	tank temp;
	int rnd=rand();
	do
	{
		flag=false;
		int a=rnd++%4;

		if(a== 0)
			temp.DR=up;

		if(a== 1)
			temp.DR=down;

		if(a== 2)
			temp.DR=_left;

		if(a== 3)
			temp.DR=_right;


		x=rand()%(width-2)+2;
		y=rand()%(hight-2)+2;

		temp.setposition(x,y);

		for(int m = x-1 ; m < x + 2 ; m++)
		{

			for(int j = y-1 ; j < y + 2 ; j++)
			{
				if(  f1->arr[j][m]==ajor || f1->arr[j][m]==beton  )//temp.beontank(x,y) &&
				{
					flag=true;
				}
				if(temp.beontank(x,y) &&  (yourtank->beontank(x,y)|| pc1->beontank(x,y) || pc2->beontank(x,y)|| pc3->beontank(x,y)) )
				{
					flag=true;
				}
			}
		}
		if(abs(x-yourtank->x)<4 || abs(y-yourtank->y<4) || abs(pc1->x-x)<4  || abs(pc1->y-y)<4  ||
			abs(pc2->x-x)<4  || abs(pc2->y-y)<4  || abs(pc3->x-x)<4 || abs(pc3->y-y)<4 )
		{
			flag=true;
		}//||yourtank->x-x<4 ||yourtank->y-y<4 ||x-pc1->x<4 ||y-pc1->y<4 ||x-pc2->x<4 ||y-pc2->y<4 ||x-pc3->x<4 ||y-pc3->y<4
	}
	while(flag ||( f1->arr[y][x]==ajor || f1->arr[y][x]==beton ||
		yourtank->beontank(x,y)|| pc1->beontank(x,y) || pc2->beontank(x,y)|| pc3->beontank(x,y) ));

	ptank->setposition(x,y);
	ptank->DR=temp.DR;
	gameframes(yourtank->DR,f1,yourtank,pc1,pc2,pc3,true);//a deafault go with no changing direction
}
//puts the tanks after coming back from the dead
void showresult(tank* yourtank,tank* pc1,tank* pc2,tank* pc3)
{
	tank* tanks[4]={yourtank,pc1,pc2,pc3};
	for( int i = 0 ; i < 4 ; i++ )
	{
		for( int j = 0 ; j < 3 ; j++ )
		{
			if(tanks[j]->point<tanks[j+1]->point)
			{
				tank* temp=tanks[j];
				tanks[j]=tanks[j+1];
				tanks[j+1]=temp;
			}
		}
	}
	for(int i=0;i<4;i++)
	{
		setcolor(black,tanks[i]->tankcol);
		cout<<tanks[i]->ID<<" point is: "<<tanks[i]->point<<endl;

	}
	setcolor(black,white);



}
//printing the points nozoli
void newgamelvl1(char  arr[hight][width],tank* yourtank,tank* pc1,tank* pc2,tank* pc3)
{
	ifstream maplvl1 ("maplvl1.txt");
	for(int j=0;!maplvl1.eof();j++)
	{
		maplvl1.get(arr[j/width][j%width]);//send the file ditails to array
	} 
	maplvl1.close();

	yourtank->setdefault();
	pc1->setdefault();
	pc2->setdefault();
	pc3->setdefault();
}
//starts game at lvl one
void go(feild* f1,tank* driver,direction D,tank* yourtank,tank* pc1,tank* pc2,tank* pc3);
// does the move for one tank
void printdetails(tank* yourtank,tank* pc1,tank* pc2,tank* pc3)
{
	int CX,CY;
	getCursorXY(CX,CY);
	gotoXY(0,hight+1);
	cout<<"                                                                                                                       *"<<endl;
	cout<<"                                                                                                                       *"<<endl;
	cout<<"                                                                                                                       *"<<endl;
	cout<<"                                                                                                                       *";
	setcolor(black,yourtank->tankcol);
	gotoXY(0,hight+1);
	cout<<"your tank life: "<<yourtank->life<<endl;
	cout<<"your tank secondchances: "<<yourtank->secondchance<<endl;
	cout<<"your tank point: "<<yourtank->point<<endl;
	cout<<"your tank range: "<<yourtank->range<<"     your tank destruction power: "<<yourtank->DS;
	tank* tanks[3]={pc1,pc2,pc3};
	for (int i=0; i<3 ;i++)
	{
		setcolor(black,tanks[i]->tankcol);
		gotoXY((i+1)*30,hight+1);
		cout<<"pc"<<i+1<<" life: "<<tanks[i]->life<<endl;
		gotoXY((i+1)*30,hight+2);
		cout<<"pc"<<i+1<<" secondchances: "<<tanks[i]->secondchance<<endl;
		gotoXY((i+1)*30,hight+3);
		cout<<"pc"<<i+1<<" point: "<<tanks[i]->point<<endl;

	}
	gotoXY(CX,CY);
	setcolor(black,white);

}
//printing detail during the game
void giveit(string str,load* load)
{
	int a=0;
	int i=load->indx;
	while (str[i]!=SLIDER)
	{
		a=a*10+(str[i]-'0');
		i++;
	}
	load->indx=i+1;
	load->value=a;
}
// for loading
void checklives(feild* f1,tank* yourtank,tank* pc1,tank* pc2,tank* pc3)
{
	if(yourtank->life<=0)
	{
		yourtank->kill(f1->arr);
		puttank(yourtank,f1,yourtank,pc1,pc2,pc3);
	}
	if(pc1->alive && pc1->life<=0)
	{

		pc1->kill(f1->arr);
		puttank(pc1,f1,yourtank,pc1,pc2,pc3);
	}
	if(pc2->alive && pc2->life<=0)
	{
		pc2->kill(f1->arr);
		puttank(pc2,f1,yourtank,pc1,pc2,pc3);
	}
	if( pc3->alive && pc3->life<=0)
	{
		pc3->kill(f1->arr);
		puttank(pc3,f1,yourtank,pc1,pc2,pc3);
	}

	if(yourtank->life<=0 && yourtank->secondchance<=0)
	{
		system("cls");
		cout<<" you lost the game"<<endl;
		endgame=true;
		showresult(yourtank,pc1,pc2,pc3);
		system("pause");
		exit(0);
	}


	if( !pc1->alive && !pc2->alive && !pc3->alive)
		//	if(0)
	{
		if(gamelvl<3)
		{
			gamelvl++;

			if(gamelvl==2)
			{
				f1->gotolvl2(yourtank,pc1,pc2,pc3);
				yourtank->setdefault();
				pc1->setdefault();
				pc2->setdefault();
				pc3->setdefault();
				puttanks(f1,yourtank,pc1,pc2,pc3);
				system("cls");
				f1->print(yourtank,pc1,pc2,pc3);
			}
			if(gamelvl==3)
			{
				f1->gotolvl3(yourtank,pc1,pc2,pc3);
				yourtank->setdefault();
				pc1->setdefault();
				pc2->setdefault();
				pc3->setdefault();
				puttanks(f1,yourtank,pc1,pc2,pc3);
				system("cls");
				f1->print(yourtank,pc1,pc2,pc3);
			}



		}
		if(gamelvl==3)
		{

			system("cls");
			cout<<" you won the game"<<endl<<"you are just lucky"<<endl;
			endgame=true;
			showresult(yourtank,pc1,pc2,pc3);
			system("pause");
			exit(0);
		}


	}
	time_t now=time(NULL);
	if(now-f1->T_DS>60 && f1->N_DS>0)// clear destruction power after 60 seconds
	{
		f1->N_DS--;
		f1->available--;
		f1->arr[f1->dsy][f1->dsx]=' ';
		int CX,CY;
		getCursorXY(CX,CY);
		gotoXY(f1->dsx,f1->dsy);
		cout<<' ';
		gotoXY(CX,CY);


	}


}
//check who must die and lvl changings
void loadkeys(keys k,tank* yourtank)
{
	ifstream readk;
	//	string keysstr;
	readk.open("keys.txt");
	//readk.get(k);
	stringstream kss;
	kss << readk.rdbuf();//read the file
	string keysstr =kss.str();//str holds the content of the file
	load loadk;

	giveit(keysstr,&loadk);
	k.up=loadk.value;
	giveit(keysstr,&loadk);
	k.down=loadk.value;
	giveit(keysstr,&loadk);
	k._right=loadk.value;
	giveit(keysstr,&loadk);
	k._left=loadk.value;
	giveit(keysstr,&loadk);
	k.shoot=loadk.value;
	giveit(keysstr,&loadk);
	k.tankcol=loadk.value;
	yourtank->tankcol=(color)k.tankcol;

}
//key that have been loaded
void loadgame(string str,feild* f1,tank* yourtank,tank* pc1,tank* pc2,tank* pc3,load L)
{
	tank* tanks[4]={yourtank,pc1,pc2,pc3};
	for ( int i=0; i < 4 ; i++)
	{
		giveit(str,&L);
		tanks[i]->alive=(bool)L.value;
		giveit(str,&L);
		tanks[i]->life=(int)L.value;
		giveit(str,&L);
		tanks[i]->secondchance=(int)L.value;
		giveit(str,&L);
		tanks[i]->DS=(int)L.value;
		giveit(str,&L);
		tanks[i]->range=(int)L.value;
		giveit(str,&L);
		tanks[i]->rangeR=(int)L.value;
		giveit(str,&L);
		tanks[i]->x=(int)L.value;
		giveit(str,&L);
		tanks[i]->y=(int)L.value;
		giveit(str,&L);
		tanks[i]->point=(int)L.value;
		giveit(str,&L);
		tanks[i]->tankcol=(color)L.value;
		giveit(str,&L);
		tanks[i]->DR=(direction)L.value;
		giveit(str,&L);
		tanks[i]->ownership=(ownership)L.value;
		giveit(str,&L);
		tanks[i]->temp=(char)L.value;
		giveit(str,&L);
		tanks[i]->bullet=(bool)L.value;
		if(tanks[i]->bullet)
		{
			giveit(str,&L);
			tanks[i]->bulletD=(direction)L.value;
			giveit(str,&L);

			tanks[i]->bx=(int)L.value;
			giveit(str,&L);
			tanks[i]->by=(int)L.value;
		}

	}
	giveit(str,&L);
	gamelvl=(int)L.value;
	int findx=L.indx;
	for(int i=0;i<hight;i++)
	{
		for(int j=0;j<width;j++)
		{
			f1->arr[i][j]=str[findx];
			findx++;
		}
	}

}
//obvois
void gameframes(direction dir,feild* f1,tank* yourtank,tank* pc1,tank* pc2,tank* pc3,bool deafault)
{
	if( yourtank->canrotate(dir,yourtank,pc1,pc2,pc3,f1->arr) && !deafault )
	{
		yourtank->cleartank();
		yourtank->DR=dir;
		yourtank->printtank();
	}
	while(!_kbhit())
	{
		checklives( f1,yourtank,pc1,pc2,pc3);
		if(time(NULL)%2==1)
		{
			printdetails(yourtank,pc1,pc2,pc3);
		}
		for(int i=0;i<4  ;i++)//&&(yourtank.bullet||pc1.bullet||pc2.bullet||pc3.bullet)
		{
			if(yourtank->bullet && yourtank->alive)
			{
				yourtank->shoot(f1->arr,yourtank,pc1,pc2,pc3);
			}


			if (pc1->bullet && pc1->alive)
			{
				pc1->shoot(f1->arr,yourtank,pc1,pc2,pc3);
			}
			else 
			{
				if(pc1->randomshoot(f1->arr,yourtank,pc1,pc2,pc3) && i==0 && pc1->alive)
				{
					pc1->shoot(f1->arr,yourtank,pc1,pc2,pc3);
				}
			}
			if (pc2->bullet && pc2->alive)
			{
				pc2->shoot(f1->arr,yourtank,pc1,pc2,pc3);
			}
			else
			{
				if(pc2->randomshoot(f1->arr,yourtank,pc1,pc2,pc3) && i==0 && pc2->alive)
				{
					pc2->shoot(f1->arr,yourtank,pc1,pc2,pc3);
				}
			}
			if (pc3->bullet && pc3->alive)
			{
				pc3->shoot(f1->arr,yourtank,pc1,pc2,pc3);
			}
			else
			{
				if(pc3->randomshoot(f1->arr,yourtank,pc1,pc2,pc3) && i==0 && pc3->alive)
				{
					pc3->shoot(f1->arr,yourtank,pc1,pc2,pc3);
				}
			}
			std::this_thread::sleep_for(std::chrono::milliseconds(BULLETSPEED));

		}
		if(yourtank->alive)
		{
			go(f1,yourtank,dir,yourtank,pc1,pc2,pc3);
		}
		if(pc1->alive)
		{
			go(f1,pc1,pc1->getrandomD(yourtank,pc1,pc2,pc3,f1->arr),yourtank,pc1,pc2,pc3);
		}
		if(pc2->alive)
		{
			go(f1,pc2,pc2->getrandomD(yourtank,pc1,pc2,pc3,f1->arr),yourtank,pc1,pc2,pc3);
		}
		if(pc3->alive)
		{
			go(f1,pc3,pc3->getrandomD(yourtank,pc1,pc2,pc3,f1->arr),yourtank,pc1,pc2,pc3);
		}
	}
}
// balla goftam
void saveit(feild* f1,tank* yourtank,tank* pc1,tank* pc2,tank* pc3)
{
	char sdate[9];
	char stime[9];
	ofstream log;
	ofstream list;
	ofstream last;
	_strdate_s(sdate);
	_strtime_s(stime);
	string filename = to_string(gamelvl)+"-";
	filename.append(stime);
	filename.append("_");
	filename.append(sdate);
	filename.append(".txt");
	for(int i = 0; i<filename.length(); ++i){
		if (filename[i] == '/' || filename[i] == ':')
			filename[i] = '-';
	}
	log.open(filename.c_str());
	if (log.fail())
	{
		perror(filename.c_str());
	}
	list.open("list.txt",ios::app);
	last.open("last.txt");
	list<<filename;
	last<<filename;
	tank tanks[4]={*yourtank,*pc1,*pc2,*pc3};
	for ( int i=0;i<4;i++)
	{
		log<<tanks[i].alive;
		log<<SLIDER;
		log<<tanks[i].life;
		log<<SLIDER;
		log<<tanks[i].secondchance;
		log<<SLIDER;
		log<<tanks[i].DS;
		log<<SLIDER;
		log<<tanks[i].range;
		log<<SLIDER;
		log<<tanks[i].rangeR;
		log<<SLIDER;
		log<<tanks[i].x;
		log<<SLIDER;
		log<<tanks[i].y;
		log<<SLIDER;
		log<<tanks[i].point;
		log<<SLIDER;
		log<<tanks[i].tankcol;
		log<<SLIDER;
		log<<tanks[i].DR;
		log<<SLIDER;
		log<<tanks[i].ownership;
		log<<SLIDER;
		log<<tanks[i].temp;
		log<<SLIDER;
		log<<tanks[i].bullet;
		log<<SLIDER;
		if(tanks[i].bullet)
		{
			log<<tanks[i].bulletD;
			log<<SLIDER;
			log<<tanks[i].bx;
			log<<SLIDER;
			log<<tanks[i].by;
			log<<SLIDER;
		}
	}
	log<<gamelvl;
	log<<SLIDER;
	for(int i=0;i<hight;i++)
	{
		for(int j=0;j<width;j++)
		{
			log<<f1->arr[i][j];
		}
	}
	log<<SLIDER;
	log.close();
	list.close();
	last.close();
}
// saves the game to a text file
void gameloop(feild* f1,tank* yourtank,tank* pc1,tank* pc2,tank* pc3,char c,keys k)
{
	if(c==k.up)
	{
		gameframes(up,f1,yourtank,pc1,pc2,pc3);
	}
	else if(c==k.down)
	{
		gameframes(down,f1,yourtank,pc1,pc2,pc3);
	}
	else if(c==k._right)
	{
		gameframes(_right,f1,yourtank,pc1,pc2,pc3);
	}
	else if( c==k._left)
	{
		gameframes(_left,f1,yourtank,pc1,pc2,pc3);
	}
	else if(c==k.shoot)// choose to shoot
	{
		if(!yourtank->bullet)
		{
			yourtank->setB(f1->arr,yourtank,pc1,pc2,pc3);
		}
		gameframes(yourtank->DR,f1,yourtank,pc1,pc2,pc3,true);//a deafault go with no changing direction
	}
	else if( c == 'p')
	{
		char p;
		do
		{
			p=_getch();
		}
		while(p!='p');
	}
	else if(c== 'q')
	{
		bool flag;
		flag=false;

		while(!flag)
		{
			system("cls");
			int CX,CY;
			getCursorXY(CX,CY);
			gotoXY(30,10);
			cout<<"GAME MENU"<<endl;
			gotoXY(30,11); cout<<"1.New Game";
			gotoXY(30,12);	cout<<"2.Saved Games";
			gotoXY(30,13); cout<<"3.Options";
			gotoXY(30,17); cout<<"q.Exit";
			gotoXY(CX,CY);

			char M;
			M=_getch();
			switch (M)
			{
			case '2':
				{
					bool stay;
					stay=true;
					while(stay)
					{
						fstream list;
						list.open("list.txt");
						string myArray;
						if(list.is_open())
						{
							for(int i = 0; !list.eof() && i<10; ++i)
							{
								list >> myArray;
							}
						}
						system("cls");
						cout<<"you have already saved below files:"<<endl;
						for(int i=0;i< 1000 &&  myArray[i]!='\0';i++)
						{
							cout<<myArray[i];
							if((i+1)%23==0)
							{
								cout<<endl;
							}
						}
						cout<<"do you want to load game or delete saved game."<<endl;
						cout<<"1.load"<<endl<<"2.delete"<<endl<<"q.menu"<<endl;
						char C;
						do
						{
							C=_getch();
						}
						while(C !='1' && C!='2'&& C!='q'); 
						if(C=='q')
						{
							break;
						}
						if(C=='2')
						{
							cout<<"enter the file name you want to delete"<<endl;
							string dlt;
							cin >> dlt;
							if(!remove(dlt.c_str()))
							{
								cout<<"it was successfully deleted"<<endl;

								std::string::size_type i = myArray.find(dlt);
								if (i != std::string::npos)
								{
									myArray.erase(i, dlt.length());
									//cout<<myArray;
								}
								remove("list.txt");
								ofstream newlist;
								newlist.open("list.txt");
								newlist<<myArray;
							}
							else
							{
								system("cls");
								cout<<"file not found"<<endl;
								system("pause");
							}
						}
						else if(C=='1')
						{
							string loadit;
							cout<<"Enter your game name to load it."<<endl;
							cin>>loadit;
							ifstream loadfile;
							loadfile.open(loadit.c_str());
							if(loadfile.is_open())
							{
								cout<<loadit<<" was loaded"<<endl;
								stringstream strStream;
								strStream << loadfile.rdbuf();//read the file
								string str = strStream.str();//str holds the content of the file
								load L;
								tank yt=*yourtank;//save some where may be user wanted to not to load the game
								tank temp1=*pc1;
								tank temp2=*pc2;
								tank temp3=*pc3;
								loadgame(str,f1,yourtank,pc1,pc2,pc3,L);
								system("cls");
								gotoXY(0,0);
								f1->print(yourtank,pc1,pc2,pc3);
								char choose;
								do
								{
									choose=_getch();
								}
								while(choose!='p' && choose!='q');
								if(choose=='q')
								{
									*yourtank=yt;
									*pc1=temp1;
									*pc2=temp2;
									*pc3=temp3;
									continue;
									//break;
								}
								if(choose=='p')
								{
									//loaded=true;
									flag=true;
									gameframes(yourtank->DR,f1,yourtank,pc1,pc2,pc3);
									break;
								}
							}
							else
							{
								system("cls");
								cout<<"File was not found"<<endl;
								system("pause");
							}
						}
						stay=false;
					}//while(stay)
					break;
			case '3':
				if(1)
				{
					system("cls");
					ofstream keys;
					keys.open("keys.txt");
					char sup,sdown,s_right,s_left,sshoot;
					int stankcol;
					cout<<"enter the key you want for \" UP \" key"<<endl;
					FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
					_getch();
					sup=_getch();
					cout<<(int)sup<<endl;
					cout<<"enter the key you want for \" DOWN \" key"<<endl;
					FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
					_getch();
					sdown=_getch();
					cout<<(int)down<<endl;
					FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
					cout<<"enter the key you want for \" RIGHT \" key"<<endl;
					FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
					_getch();
					s_right=_getch();
					cout<<(int)s_right<<endl;
					FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
					cout<<"enter the key you want for \" LEFT \" key"<<endl;
					FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
					_getch();
					s_left=_getch();
					cout<<(int)s_left<<endl;
					FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
					cout<<"enter the key you want for \" SHOOTE \" key"<<endl;
					FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
					_getch();
					sshoot=_getch();
					FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
					cout<<"enter the code color you want for YOUR TANK COLOR"<<endl;
					cout<<"blue=9,black=0,green=10,white=7,red=12,lightblue=11,yellow=14,gray=8,purple=13,"<<endl;
					cin>>stankcol;

					keys<<(int)sup;
					keys<<SLIDER;
					keys<<(int)sdown;
					keys<<SLIDER;
					keys<<(int)s_right;
					keys<<SLIDER;
					keys<<(int)s_left;
					keys<<SLIDER;
					keys<<(int)sshoot;
					keys<<SLIDER;
					keys<<stankcol;
					keys<<SLIDER;

					keys.close();
					loadkeys(k,yourtank);
					system("cls");
					f1->print(yourtank,pc1,pc2,pc3);
				}
				flag=true;

				break;
			case 'q':
				exit(0);
				flag=true;
				break;
			case '1':

				system("cls");
				newgamelvl1(f1->arr,yourtank,pc1,pc2,pc3);
				puttanks(f1,yourtank,pc1,pc2,pc3);
				f1->N_DS=0;
				f1->N_SC=0;
				f1->available=0;
				f1->print(yourtank,pc1,pc2,pc3);
				flag=true;
				int p;
				do
				{
					p=_getch();
				}while(p!='p');
				gameframes(yourtank->DR,f1,yourtank,pc1,pc2,pc3,true);//a deafault go with no changing direction
				break;
			default:
				flag=false;
				break;
				}

			}
		}//while(!flag)
	}
	else if(c=='l')//direct load
	{
		ifstream loadlast;
		loadlast.open("last.txt");
		stringstream ssload;
		ssload << loadlast.rdbuf();//read the file
		string laststr = ssload.str();//str holds the content of the file



		ifstream loadfile;
		loadfile.open(laststr.c_str());
		if(loadfile.is_open())
		{
			cout<<loadlast<<" was loaded"<<endl;

			stringstream strStream;
			strStream << loadfile.rdbuf();//read the file
			string str = strStream.str();//str holds the content of the file
			load L;
			tank yt=*yourtank;//save some where may be user wanted to not to load the game
			tank temp1=*pc1;
			tank temp2=*pc2;
			tank temp3=*pc3;
			loadgame(str,f1,yourtank,pc1,pc2,pc3,L);

			system("cls");
			gotoXY(0,0);
			f1->print(yourtank,pc1,pc2,pc3);
			char stop;
			do
			{
				stop=_getch();
			}while(stop!='p');
			gameframes(yourtank->DR,f1,yourtank,pc1,pc2,pc3);//a deafault go with no changing direction
		}
	}

	else if(c== 's')
	{

		saveit(f1,yourtank,pc1,pc2,pc3);

		gameframes(yourtank->DR,f1,yourtank,pc1,pc2,pc3,true);//a deafault go with no changing direction
	}//else if(c== 's')
	else
	{
		gameframes(yourtank->DR,f1,yourtank,pc1,pc2,pc3,true);//a deafault go with no changing direction

	}
}
//taking your ordrs an deciding what to do
direction dontgo(direction dir,direction dir1=NULL_dir)
{
	direction dir2=NULL_dir;
		int rnd=rand();
	do
	{
		int a=rnd++%4;

		if(a==0)
		{
			dir2=up;
		}
		if(a==1)
		{
			dir2=down;
		}
		if(a==2)
		{
			dir2=_left;
		}
		if(a==3)
		{
			dir2=_right;
		}
	}while(dir2==dir || dir2==dir1);
	return dir2;
}
// for navigation
direction find(int sx,int sy,char arr[hight][width])
{
	bool flag=false;
	bool breakflag=false;
	int x,y;
	for (int i = sx-7 ; i < sx + 8 && i < width-2 && 0 < i ; i++ )
	{
		for ( int j = sy-7 ; j < sy + 8 && j < hight-2 && 0 < j ; j++ )
		{
			if(arr[j][i]=='S' || arr[j][i]=='L' || arr[j][i]=='R' || arr[j][i]=='d')
			{
				x=i;
				y=j;
				flag=true;
				breakflag=true;
				break;

			}
		}
		if(breakflag)
		{
			break;
		}
	}
	if(flag)
	{
		if( x>sx && abs(sy-y)<=abs(sx-x) )
		{
			return _right;

		}
		if( x<sx && abs(sy-y)<=abs(sx-x) )
		{
			return _left;

		}
		if( y<sy && abs(sy-y)>abs(sx-x) )
		{
			return up;
		}
		if( y>sy && abs(sy-y)>abs(sx-x) )
		{
			return down;
		}
	}
	else
	{
		return NULL_dir;
	}
}
// for getting sa
int _tmain(int argc, _TCHAR* argv[])
{


	srand(time(NULL));
	tank yourtank;
	yourtank.tankcol=green;
	yourtank.address=&yourtank;
	yourtank.DR=_right;
	yourtank.ownership=player;
	feild f1;
	ifstream maplvl1 ("maplvl1.txt");

	for(int j=0;!maplvl1.eof();j++)
	{
		maplvl1.get(f1.arr[j/width][j%width]);//send the file ditails to array

	}

	maplvl1.close();
	keys k;

	loadkeys(k,&yourtank);

	tank pc1;
	tank pc2;
	tank pc3;
	pc1.address=&pc1;
	pc2.address=&pc2;
	pc3.address=&pc3;

	pc1.tankcol=red;
	pc1.DR=up;
	pc2.tankcol=lightblue;
	pc2.DR=down;
	pc3.tankcol=blue;
	pc3.DR=_left;

	pc1.ownership=pc;
	pc2.ownership=pc;
	pc3.ownership=pc;
	yourtank.ID="yourtank";
	pc1.ID="pc1";
	pc2.ID="pc2";
	pc3.ID="pc3";



	gameloop(&f1,&yourtank,&pc1,&pc2,&pc3,'q',k);




	while(!endgame)//////////////////////////////////////////////////////////////////////
	{


		checklives(& f1,&yourtank,&pc1,&pc2,&pc3);


		if(f1.available<2)
		{
			f1.putSA(&yourtank,&pc1,&pc2,&pc3);
		}

		char c=_getch();
		printdetails(&yourtank,&pc1,&pc2,&pc3);

		gameloop(&f1,&yourtank,&pc1,&pc2,&pc3,c,k);

	}

	system("pause");
	return 0;
}

void go(feild* f1,tank* driver,direction D,tank* yourtank,tank* pc1,tank* pc2,tank* pc3)
{

	tank temp;
	temp.x=driver->x;
	temp.y=driver->y;
	temp.DR=D;
	if (D==up)
	{
		temp.y--;
	}
	else if (D==down)
	{
		temp.y++;
	}
	else if (D==_left)
	{
		temp.x--;
	}
	else if (D==_right)
	{
		temp.x++;
	}
	for (int i=temp.x-1;i<temp.x+2;i++)
	{
		for (int j=temp.y-1;j<temp.y+2;j++)
		{
			if ( temp.beontank(i,j) && ( f1->arr[j][i]==ajor || f1->arr[j][i]==beton ) )
			{
				return;
			}
			if((pc1->beontank(i,j) && temp.beontank(i,j) && pc1!=driver) || 
				(pc2->beontank(i,j) && temp.beontank(i,j) && pc2!=driver)||
				(pc3->beontank(i,j) && temp.beontank(i,j) && pc3!=driver) || 
				(yourtank->beontank(i,j) && temp.beontank(i,j) && yourtank!=driver))
			{
				return;
			}


		}
	}

	if(driver->alive==false)
	{
		return;
	}

	else if ( D==up && !(pc1->beontank(driver->x,driver->y-2 )&& driver!=pc1)&& !(pc2->beontank(driver->x,driver->y-2) && driver!=pc2)
		&& !( pc3->beontank(driver->x,driver->y-2) && driver!=pc3) && !(yourtank->beontank(driver->x,driver->y-2) && driver!=yourtank ) )
	{
		if(driver->y > 2 && f1->arr[driver->y-2][driver->x]!=ajor)
		{
			driver->cleartank();
			driver->y--;
			driver->printtank();


		}
		//	DR=up;
	}
	else if (D==down  && !( pc1->beontank(driver->x,driver->y+2) && driver!=pc1)&& !(pc2->beontank(driver->x,driver->y+2) && driver != pc2 )
		&& !(pc3->beontank(driver->x,driver->y+2) && driver != pc3)&& !(yourtank->beontank(driver->x,driver->y+2) && driver !=yourtank) )
	{
		if(driver->y<hight-3 && f1->arr[driver->y+2][driver->x]!=ajor)
		{
			driver->cleartank();
			driver->y++;
			driver->printtank();
		}
		//	DR=down;
	}



	else if (D==_right && !(pc1->beontank(driver->x+2,driver->y) && driver != pc1)&& !(pc2->beontank(driver->x+2,driver->y) && driver!=pc2)
		&& !( pc3->beontank(driver->x+2,driver->y) && driver != pc3) && !(yourtank->beontank(driver->x+2,driver->y) && driver!=yourtank)  )
	{
		if(driver->x<width-3 && f1->arr[driver->y][driver->x+2]!=ajor)
		{
			driver->cleartank();

			driver->x++;
			driver->printtank();
		}
		//	DR=_right;
	}
	else if (D==_left && !(pc1->beontank(driver->x-2,driver->y) && driver!=pc1)&& !(pc2->beontank(driver->x-2,driver->y) && driver!=pc2)&&
		!( pc3->beontank(driver->x-2,driver->y) && driver!=pc3 ) && !( yourtank->beontank(driver->x-2,driver->y) && driver!=yourtank) )
	{
		if(driver->x > 2 && f1->arr[driver->y][driver->x-2]!=ajor)
		{
			driver->cleartank();
			driver->x--;
			driver->printtank();
		}
		//	DR=_left;
	}

	for(int i = driver->x - 1 ; i < driver->x + 2 ; i++)
	{
		//	bool breakflag=false;
		for(int j = driver->y - 1 ; j < driver->y + 2 ; j++)
		{
			if(f1->performA(driver->address,f1->getSA(i,j))==true )
			{
				f1->arr[j][i]=' ';
				//breakflag=true;
				break;
			}
		}
		//	if(breakflag)
		//{
		//	break;
		//}
	}


}

bool tank::beontank(int xo,int yo)
{
	if(DR==down)
	{
		if(xo==x && yo==y)
		{
			return true;
		}
		else if(xo==x-1 && yo==y)
		{
			return true;
		}
		else if(xo==x+1 && yo==y)
		{
			return true;
		}
		else if(xo==x && yo==y+1)
		{
			return true;
		}
		else if(xo==x-1 && yo==y-1)
		{
			return true;
		}
		else if(xo==x+1 && yo==y-1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else if(DR==up)
	{
		if(xo==x && yo==y)
		{
			return true;
		}
		else if(xo==x-1 && yo==y)
		{
			return true;
		}
		else if(xo==x+1 && yo==y)
		{
			return true;
		}
		else if(xo==x && yo==y-1)
		{
			return true;
		}
		else if(xo==x-1 && yo==y+1)
		{
			return true;
		}
		else if(xo==x+1 && yo==y+1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	else if(DR==_right)
	{
		if(xo==x && yo==y)
		{
			return true;
		}
		else if(xo==x && yo==y+1)
		{
			return true;
		}
		else if(xo==x && yo==y-1)
		{
			return true;
		}
		else if(xo==x+1 && yo==y)
		{
			return true;
		}
		else if(xo==x-1 && yo==y-1)
		{
			return true;
		}
		else if(xo==x-1 && yo==y+1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else if(DR==_left)
	{
		if(xo==x && yo==y)
		{
			return true;
		}
		else if(xo==x && yo==y+1)
		{
			return true;
		}
		else if(xo==x && yo==y-1)
		{
			return true;
		}
		else if(xo==x-1 && yo==y)
		{
			return true;
		}
		else if(xo==x+1 && yo==y-1)
		{
			return true;
		}
		else if(xo==x+1 && yo==y+1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}

}
// mohavate tank
void tank::cleartank()
{
	for(int i=x-4;i<x+4;i++)
	{
		for(int j=y-4;j<y+4;j++)
		{
			if(beontank(i,j))
			{
				int CX,CY;
				getCursorXY(CX,CY);
				gotoXY(i,j);
				cout<<' ';
				gotoXY(CX,CY);
			}
		}
	}
}

void tank::printtank()
{
	for(int i=x-4;i<x+4;i++)
	{
		for(int j=y-4;j<y+4;j++)
		{
			if(beontank(i,j))
			{
				int CX,CY;
				getCursorXY(CX,CY);
				gotoXY(i,j);
				setcolor(tankcol,white);
				cout<<' ';
				setcolor(black,white);
				gotoXY(CX,CY);
			}
		}
	}
}

bool tank::canrotate(direction D,tank* yourtank,tank* pc1,tank* pc2,tank* pc3,char arr [hight][width])
{

	tank temp;
	temp.x=x;
	temp.y=y;
	temp.DR=D;
	for (int i=temp.x-1;i<temp.x+2;i++)
	{
		for (int j=temp.y-1;j<temp.y+2;j++)
		{
			if(temp.beontank( i , j ) && pc1->beontank( i , j ) && address!=pc1)
			{
				return false;
			}

			if(temp.beontank( i , j ) &&  pc2->beontank( i , j ) && address!=pc2)
			{
				return false;
			}
			if(temp.beontank( i , j ) &&  pc3->beontank( i , j ) && address!=pc3)
			{
				return false;
			}
			if(temp.beontank( i , j ) &&  yourtank->beontank( i , j ) && address!=yourtank)
			{
				return false;
			}
			if (temp.beontank( i , j ) && ( arr[j][i]==ajor || arr[j][i]==beton))
			{
				return false;
			}

		}
	}

	return true;
}
//for navigation
direction tank::getrandomD(tank* yourtank,tank* pc1,tank* pc2,tank* pc3,char arr [hight][width])
{
	

	/*if( (yourtank->y==y-2 || yourtank->y==y-1) &&  (yourtank->x==x-1 || yourtank->x==x-2) )
	{
		return dontgo(up,_left);
	}
	if( (yourtank->y==y-2||yourtank->y==y-1) && ( yourtank->x==x+2|| yourtank->x==x+1) )
	{
		return dontgo(up,_right);
	}
	if( (yourtank->y==y+2 || yourtank->y==y+1) &&  (yourtank->x==x-2||yourtank->x==x-1) )
	{
		return dontgo(down,_left);
	}
	if( (yourtank->y==y+2 || yourtank->y==y+1) &&  (yourtank->x==x+2 || yourtank->x==x+1) )
	{
		return dontgo(down,_right);
	}*/
	tank* tanks[4]={yourtank,pc1,pc2,pc3};
	for(int i=0;i<3;i++)
	{

		if( (tanks[i]->y==y-2 || tanks[i]->y==y-1 || tanks[i]->y==y ) && ( tanks[i]->x==x-1 || tanks[i]->x==x-2|| tanks[i]->x==x) && tanks[i]->address!=address)
		{
			return dontgo(up,_left);
		}
		if( (tanks[i]->y==y-2||tanks[i]->y==y-1||tanks[i]->y==y) && ( tanks[i]->x==x+2|| tanks[i]->x==x+1 || tanks[i]->x==x) && tanks[i]->address!=address)
		{
			return dontgo(up,_right);
		}
		if( (tanks[i]->y==y+2 || tanks[i]->y==y+1|| tanks[i]->y==y) && (tanks[i]->x==x-2||tanks[i]->x==x-1||tanks[i]->x==x) && tanks[i]->address!=address)
		{
			return dontgo(down,_left);
		}
		if( (tanks[i]->y==y+2 || tanks[i]->y==y+1|| tanks[i]->y==y) &&  (tanks[i]->x==x+2 || tanks[i]->x==x+1|| tanks[i]->x==x) && tanks[i]->address!=address)
		{
			return dontgo(down,_right);
		}
		
	}







	if(arr[y+2][x+2]==ajor && arr[y-2][x+2]==ajor)
	{
		int a=rand()%3;
		if(a==0)
		{
			return up;
		}else if(a==1)
		{
			return _left;
		}else if(a==2)
		{
			return down;
		}
	}
	if(arr[y+2][x-2]==ajor && arr[y-2][x-2]==ajor)
	{
		int a=rand()%3;
		if(a==0)
		{
			return up;
		}else if(a==1)
		{
			return down;
		}else if(a==2)
		{
			return _right;
		}
	}
	if(arr[y+2][x-2]==ajor && arr[y+2][x+2]==ajor)
	{
		int a=rand()%3;
		if(a==0)
		{
			return up;
		}else if(a==1)
		{
			return _left;
		}else if(a==2)
		{
			return _right;
		}
	}
	if(arr[y-2][x-2]==ajor && arr[y-2][x+2]==ajor)
	{
		int a=rand()%3;
		if(a==0)
		{
			return down;
		}else if(a==1)
		{
			return _left;
		}else if(a==2)
		{
			return _right;
		}
	}

	if(arr[y+1][x+1]==ajor)
	{
		int a=rand()%2;
		if(a)
		{
			return up;
		}else
		{
			return _left;
		}

	}
	if(arr[y-1][x+1]==ajor)
	{
		int a=rand()%2;
		if(a)
		{
			return down;
		}else
		{
			return _left;
		}

	}

	if(arr[y+1][x-1]==ajor)
	{
		int a=rand()%2;
		if(a)
		{
			return up;
		}else
		{
			return _right;
		}

	}
	if(arr[y-1][x-1]==ajor)
	{
		int a=rand()%2;
		if(a)
		{
			return down;
		}else
		{
			return _right;
		}

	}

	if( find(x,y,arr)!=NULL_dir )
	{
		return find(x,y,arr);

	}



	if(  yourtank->y <= y  &&   yourtank->x >= x )//up and right&&&&
	{
		int a=rand()%2;
		if( canrotate(up,yourtank,pc1,pc2,pc3,arr) && arr[y-2][x]!=ajor &&
			canrotate(_right,yourtank,pc1,pc2,pc3,arr)&& arr[y][x+2]!=ajor)// can go both
		{
			if(y-yourtank->y >  yourtank->x - x)
			{
				cleartank();
				DR=up;
				return up;
			}
			else 
			{
				cleartank();
				DR=_right;
				return	_right;
			}
		}
		if( canrotate(up,yourtank,pc1,pc2,pc3,arr) && arr[y-2][x]!=ajor)
		{
			cleartank();
			DR=up;
			return up;
		}
		else if (canrotate(_right,yourtank,pc1,pc2,pc3,arr)&& arr[y][x+2]!=ajor)
		{
			cleartank();
			DR=_right;
			return	_right;
		}
	}




	if( yourtank->y < y && yourtank->x < x )//up and left
	{

		if(canrotate(up,yourtank,pc1,pc2,pc3,arr)  && arr[y-2][x]!=ajor &&
			canrotate(_left,yourtank,pc1,pc2,pc3,arr)  && arr[y][x-2]!=ajor )//up and left booth
		{

			if(y-yourtank->y >  x-yourtank->x)
			{
				cleartank();
				DR=up;
				return up;
			}
			else
			{
				cleartank();
				DR=_left;
				return _left;
			}
		}
		if(canrotate(up,yourtank,pc1,pc2,pc3,arr)  && arr[y-2][x]!=ajor)
		{
			cleartank();
			DR=up;
			return up;
		}

		if(canrotate(_left,yourtank,pc1,pc2,pc3,arr)  && arr[y][x-2]!=ajor)
		{
			cleartank();
			DR=_left;
			return _left;
		}
	}



	if( yourtank->y > y  &&   yourtank->x > x )//down and right
	{

		if(canrotate(down,yourtank,pc1,pc2,pc3,arr) && arr[y+2][x]!=ajor  && 
			canrotate(_right,yourtank,pc1,pc2,pc3,arr)  && arr[y][x+2]!=ajor)//down and right both

		{
			int a=rand()%2;
			if(yourtank->y-y >  yourtank->x-x)
			{
				cleartank();
				DR=down;
				return down;
			}
			else {
				cleartank();
				DR=_right;
				return	_right;
			}
		}
		if(canrotate(down,yourtank,pc1,pc2,pc3,arr) && arr[y+2][x]!=ajor )
		{
			cleartank();
			DR=down;
			return down;
		}
		if(canrotate(_right,yourtank,pc1,pc2,pc3,arr)  && arr[y][x+2]!=ajor)
		{
			cleartank();
			DR=_right;
			return	_right;
		}
	}
	if( yourtank->y >= y  &&  yourtank->x <= x )//down and left

		if(canrotate(down,yourtank,pc1,pc2,pc3,arr)  && arr[y+2][x]!=ajor  && 
			canrotate(_right,yourtank,pc1,pc2,pc3,arr) && arr[y][x+2]!=ajor)//down and left both
		{
			int a=rand()%2;
			if(yourtank->y-y >  x-yourtank->x)
			{
				cleartank();
				DR=down;
				return down;
			}
			else {
				cleartank();
				DR=_left;
				return _left;
			}
		}
		if(canrotate(down,yourtank,pc1,pc2,pc3,arr)  && arr[y+2][x]!=ajor )
		{
			cleartank();
			DR=down;
			return down;
		}
		if(canrotate(_right,yourtank,pc1,pc2,pc3,arr) && arr[y][x+2]!=ajor)
		{
			cleartank();
			DR=_left;
			return _left;
		}
}
// for navigation 
void tank::clearwall(int WX,int WY,char arr [hight][width])
{
	int CX,CY;
	getCursorXY(CX,CY);
	gotoXY(WX,WY);
	cout<<' ';
	gotoXY(CX,CY);
}

inline void tank::setposition(int inpx,int inpy)
{
	x=inpx;
	y=inpy;
}

void tank::clearB(char arr [hight][width])//clear bullet
{
	//temp=arr[by][bx];
	if( !( bx>width-1 ||bx<1 || by<1 ||by>hight-1) && ( temp=='L' || temp=='S'|| temp=='d' || temp=='R' ) )
	{
		arr[by][bx]=(char)temp;//blank space
	}
	else if( !( bx>width-1 ||bx<1 || by<1 ||by>hight-1) && !( temp=='L' || temp=='S'|| temp=='d' || temp=='R' ) )
	{
		arr[by][bx]=' '; 
	}
	int CX,CY;
	getCursorXY(CX,CY);
	gotoXY(bx,by);
	if(temp=='L' ||temp=='S'||temp=='d' || temp=='R' )
	{
		cout<<(char)temp;
	}
	else
	{
		cout<<' ';
	}
	gotoXY(CX,CY);
}
//clear bullet
void tank::printB(char arr [hight][width])
{
	int CX,CY;
	getCursorXY(CX,CY);
	arr[by][bx]=b;
	gotoXY(bx,by);
	cout<<arr[by][bx];
	gotoXY(CX,CY);

}
//printing bullet
void tank::setB(char arr [hight][width],tank* yourtank,tank* pc1,tank* pc2,tank* pc3)
{
	if(alive==false)
	{
		return;
	}
	rangeR=range;//rangeR means remained range
	bulletD=DR;
	//bullet=true;

	if(bulletD==up && y>2)
	{
		by=y-2;
		bx=x;
		bullet=true;
	}
	else if(bulletD==down && y<hight-3)
	{
		by=y+2;
		bx=x;
		bullet=true;
	}
	else if(bulletD==_right && x<width-3)
	{
		by=y;
		bx=x+2;
		bullet=true;
	}
	else if(bulletD==_left && x>2)
	{
		by=y;
		bx=x-2;	
		bullet=true;
	}
	if(ownership==player && pc1->beontank(bx,by))
	{
		clearB(arr);
		bullet=false;
		point+=20;
		pc1->point-=10;
		pc1->life-=DS;

	}
	else if(ownership==player && pc2->beontank(bx,by))
	{
		clearB(arr);
		bullet=false;
		point+=20;
		pc2->point-=10;
		pc2->life-=DS;
	}
	else if( ownership==player && pc3->beontank( bx , by ) )
	{
		clearB(arr);
		bullet=false;
		point+=20;
		pc3->point-=10;
		pc3->life-=DS;
	}
	else if ( ownership==pc && yourtank->beontank( bx , by ) )
	{
		clearB(arr);
		bullet=false;
		point+=20;
		yourtank->point-=10;
		yourtank->life-=DS;
	}



}
// putting bullet
void tank::shoot(char arr [hight][width],tank* yourtank,tank* pc1,tank* pc2,tank* pc3)
{
	if( bx>width-1 ||bx<1 || by<1 ||by>hight-1)
	{
		//clearB(arr);
		bullet=false;
		return;

	}
	if(alive==false)
	{
		if(bullet)
		{
			clearB(arr);
		}
		return;
	}

	//bx and by ---->  bullet X and Y
	if(bulletD==up )
	{
		if(ownership==player && pc1->beontank(bx,by-1))
		{


			clearB(arr);
			bullet=false;
			point+=20;
			pc1->point-=10;
			pc1->life-=DS;

		}
		else if(ownership==player && pc2->beontank(bx,by-1))
		{
			//arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
			point+=20;
			pc2->point-=10;
			pc2->life-=DS;
		}
		else if(ownership==player && pc3->beontank(bx,by-1))
		{
			//arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
			point+=20;
			pc3->point-=10;
			pc3->life-=DS;
		}
		else if ( ownership==pc && yourtank->beontank( bx , by - 1 ) )
		{
			//	arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
			point+=20;
			yourtank->point-=10;
			yourtank->life-=DS;
		}
		if(by>1 &&  rangeR>0 && arr[by-1][bx]!=ajor)
		{

			clearB(arr);
			//	arr[by][bx]=' ';
			by--;
			rangeR--;
			temp=arr[by][bx];
			printB(arr);

		}
		if (arr[by-1][bx]==ajor)
		{
			//arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
			arr[by-1][bx]=' ';
			clearwall(bx,by-1,arr);

		}
		if(by==1 || rangeR<=0)
		{
			//arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
		}
	}


	else if(bulletD==down )/////////////////DDDDD OOOOO WWWWWWWW NNNNNNNN
	{
		if(ownership==player && pc1->beontank(bx,by+1))
		{
			//arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
			point+=20;
			pc1->point-=10;
			pc1->life-=DS;
		}
		else	if(ownership==player && pc2->beontank(bx,by+1))
		{
			//arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
			point+=20;
			pc2->point-=10;
			pc2->life-=DS;
		}
		else if(ownership==player && pc3->beontank(bx,by+1))
		{
			arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
			point+=20;
			pc3->point-=10;
			pc3->life-=DS;
		}
		else if(ownership==pc && yourtank->beontank(bx,by+1))
		{
			//	arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
			point+=20;
			yourtank->point-=10;
			yourtank->life-=DS;

		}
		if(by<hight-2 &&  rangeR>0 && arr[by+1][bx]!=ajor)
		{
			clearB(arr);
			//arr[by][bx]=' ';

			by++;

			arr[by][bx]=b;
			printB(arr);
			rangeR--;

		}
		if (arr[by+1][bx]==ajor)
		{
			//arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
			arr[by+1][bx]=' ';
			clearwall(bx,by+1,arr);

		}
		if(by==hight-2 || rangeR<=0)
		{
			//arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
		}
	}
	else if(bulletD==_right )/////////////////////////////////////////////
	{
		if(ownership==player && pc1->beontank(bx+1,by))
		{
			//	arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
			point+=20;
			pc1->point-=10;
			pc1->life-=DS;
		}
		else if(ownership==player && pc2->beontank(bx+1,by))
		{
			//arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
			point+=20;
			pc2->point-=10;
			pc2->life-=DS;
		}
		else if(ownership==player && pc3->beontank(bx+1,by))
		{
			//arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
			point+=20;
			pc3->point-=10;
			pc3->life-=DS;
		}
		else if(ownership==pc && yourtank->beontank(bx+1,by))
		{
			//arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
			point+=20;
			yourtank->point-=10;
			yourtank->life-=DS;

		}
		if(bx<width-2 &&  rangeR>0 && arr[by][bx+1]!=ajor)
		{
			clearB(arr);
			//	arr[by][bx]=' ';


			bx++;


			rangeR--;
			temp=arr[by][bx];
			printB(arr);

		}
		if (arr[by][bx+1]==ajor)
		{
			//arr[by][bx]=' ';
			clearB(arr);

			arr[by][bx+1]=' ';
			clearwall(bx+1,by,arr);
			bullet=false;
		}
		if(bx==width-2 || rangeR<=0 )
		{
			//	arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
		}
	}
	else if(bulletD==_left)/////////////////////////////
	{

		if(ownership==player && pc1->beontank(bx-1,by))
		{
			//arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
			point+=20;
			pc1->point-=10;
			pc1->life-=DS;
		}
		else if(ownership==player && pc2->beontank(bx-1,by))
		{
			//arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
			point+=20;
			pc2->point-=10;
			pc2->life-=DS;
		}
		else if(ownership==player && pc3->beontank(bx-1,by))
		{
			//arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
			point+=20;
			pc3->point-=10;
			pc3->life-=DS;
		}
		else if(ownership==pc && yourtank->beontank(bx-1,by))
		{
			//	arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
			point+=20;
			yourtank->point-=10;
			yourtank->life-=DS;

		}
		if(bx>1 &&  rangeR>0 && arr[by][bx-1]!=ajor)
		{
			clearB(arr);
			//arr[by][bx]=' ';
			bx--;
			temp=arr[by][bx];
			rangeR--;
			printB(arr);

		}
		if (arr[by][bx-1]==ajor)
		{
			//	arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
			arr[by][bx-1]=' ';
			clearwall(bx-1,by,arr);
		}

		if(bx==1 || rangeR<=0)
		{
			//	arr[by][bx]=' ';
			clearB(arr);
			bullet=false;
		}

	}
	if(bx==yourtank->bx && by==yourtank->by && address!=yourtank && bullet==true && yourtank->bullet==true)
	{
		clearB(arr);
		yourtank->clearB(arr);
		bullet=false;
		yourtank->bullet=false;

	}
}
//shooting bullet
bool tank::randomshoot(char arr [hight][width],tank* yourtank,tank* pc1,tank* pc2,tank* pc3) 
{

	int a=rand()%2;
	if(a==1)
	{
		setB(arr,yourtank,pc1,pc2,pc3);
		//	shoot(arr,yourtank,pc1,pc2,pc3);
		return true;
	}
	else if(a==0)
	{
		return false;
	}
}
// decide to shoote
void tank::kill(char arr [hight][width])
{
	if(secondchance<=0 && alive)
	{
		alive=false;
		bullet=false;
		cleartank();
		setposition(width+6,4*(N+1));
		N++;
		if( bx>width-1 ||bx<1 || by<1 ||by>hight-1)
		{
			//	clearB(arr);
			bullet=false;
			return;

		}
		else 
		{
			clearB(arr);
		}

	}
	else if(secondchance>0)
	{
		bullet=false;
		clearB(arr);
		cleartank();
		secondchance--;
		life=100;
		setposition(10,10);

	}
}
// killing the tank

